const { SoccerLinks } = require('./soccerutils/links');
const pathParams = require('../soccer/soccerutils/path.parameters');
const router = require('express').Router();
const SportmonksApi = require('./soccerutils/sports.monks').SportmonksApi

router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
});

const token = process.env.SOCCER_API_TOKEN

//beginregion getAllLeagues
const getAllLeagues = (apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        season: true,
        country: true
    };
    api.get(SoccerLinks.allLeagues, queryParams)
        .then(response => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route("/all").get((req, res) => {
    getAllLeagues((response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    })
})
//endregion

//beginregion getLeagueById
const getLeagueById = (leagueId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        season: true,
        country: true
    };
    queryParams[pathParams.LEAGUE_ID] = leagueId
    api.get(SoccerLinks.leagueById, queryParams)
        .then(response => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/id/:leagueId').get((req, res) => {
    const { leagueId } = req.params;
    getLeagueById(leagueId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

//beginregion getLeagueByCountryId
const getLeaguesByCountryId = (countryId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        season: true,
        country: true
    };
    queryParams[pathParams.COUNTRY_ID] = countryId
    api.get(SoccerLinks.leaguesByCountryId, queryParams)
        .then(response => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/countryId/:countryId').get((req, res) => {
    const { countryId } = req.params;
    getLeaguesByCountryId(countryId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

//beginregion getLeagueByName
const getLeagueByName = (leagueName, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        season: true,
        country: true
    };
    queryParams[pathParams.LEAGUE_NAME] = leagueName;
    api.get(SoccerLinks.leagueByName, queryParams)
        .then(response => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/name/:leagueName').get((req, res) => {
    const { leagueName } = req.params;
    getLeagueByName(leagueName, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

module.exports = router;