const { SoccerLinks } = require('./soccerutils/links');
const pathParams = require('../soccer/soccerutils/path.parameters');
const router = require('express').Router();
const SportmonksApi = require('./soccerutils/sports.monks').SportmonksApi

router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
});
const token = process.env.SOCCER_API_TOKEN
//region getStandingsBySeasonId
const getStandingsBySeasonId = (seasonId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {};
    queryParams['standings.team'] = true;
    queryParams[pathParams.SEASON_ID] = seasonId;
    api.get(SoccerLinks.standingsBySeasonId, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
}

router.route('/:seasonId').get((req, res) => {
    const { seasonId } = req.params;
    getStandingsBySeasonId(seasonId, (response, error) => {
        if (response) {
            // console.log(`response for getSeasonById gotten ${response}`);
            res.header["Content-Type"] = "application/json"
            res.end(response);
        } else if (error) {
            res.end('Error ',error);
        }
    });

});
//endregion

//region getStandingsBySeasonIdAndDate
const getStandingsBySeasonIdAndDate = (seasonId, date, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {};
    queryParams[pathParams.DATE_TIME] = date;
    queryParams[pathParams.SEASON_ID] = seasonId;
    api.get(SoccerLinks.standingsBySeasonIdAndDate, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
}

router.route('/:seasonId/date/:date').get((req, res) => {
    const { seasonId, date } = req.params;
    getStandingsBySeasonIdAndDate(seasonId, date, (response, error) => {
        if (response) {
            // console.log(`response for getSeasonById gotten ${response}`);
            res.header["Content-Type"] = "application/json"
            res.end(response);
        } else if (error) {
            res.end('Error ',error);
        }
    });

});
//endregion
//region getStandingsCorrectionsBySeasonId
const getStandingsCorrectionsBySeasonId = (seasonId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {};
    queryParams[pathParams.SEASON_ID] = seasonId;
    api.get(SoccerLinks.standingsCorrectionsBySeasonId, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
}

router.route('/corrections/:seasonId').get((req, res) => {
    const { seasonId } = req.params;
    getStandingsCorrectionsBySeasonId(seasonId, (response, error) => {
        if (response) {
            // console.log(`response for getSeasonById gotten ${response}`);
            res.header["Content-Type"] = "application/json"
            res.end(response);
        } else if (error) {
            res.end('Error ',error);
        }
    });

});
//endregion

module.exports = router;