const { path } = require('express/lib/application');
const { SoccerLinks } = require('./soccerutils/links');
const pathParams = require('./soccerutils/path.parameters');
const router = require('express').Router();
const SportmonksApi = require('./soccerutils/sports.monks').SportmonksApi

router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
});
const token = process.env.SOCCER_API_TOKEN || 'wk3zCBtjHq62KjZsuXjb4RDdOzZ9CQWA1a0PSPj7HFRuOLQCpDvuf3SglOUM'
//region getPlayerById
const getPlayerById = (playerId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        team: true
    };
    queryParams[pathParams.PLAYER_ID] = playerId;
    api.get(SoccerLinks.playerById, queryParams)
        .then(response => apiResponseCallback(JSON.stringify(response), null))
        .catch(error => apiResponseCallback(null, error));
};
router.route('/id/:playerId').get((req, res) => {
    const { playerId } = req.params;
    console.log('Hit playerById endpoint ');
    getPlayerById(playerId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion
//redion getPlayerByCountryId
const getPlayersByCountryId = (countryId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        team: true,
        position: true
    };
    queryParams[pathParams.COUNTRY_ID] = countryId;
    api.get(SoccerLinks.playersForCountryId, queryParams)
        .then(response => apiResponseCallback(JSON.stringify(response), null))
        .catch(error => apiResponseCallback(null, error));
}

router.route('/countryId/:countryId').get((req, res) => {
    const { countryId } = req.params;
    console.log('Hit playersByCountryId endpoint 4 country Id ', countryId);
    getPlayersByCountryId(countryId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
})
//endregion

//region getPlayerByName
const getPlayerByName = (playerName, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        team: true
    };
    queryParams[pathParams.PLAYER_NAME] = playerName;
    api.get(SoccerLinks.playerByName, queryParams)
        .then(response => apiResponseCallback(JSON.stringify(response), null))
        .catch(error => apiResponseCallback(null, error));
};

router.route('/name/:playerName').get((req, res) => {
    const { playerName } = req.params;
    console.log('Hit playerById endpoint 4 player ', playerName);
    getPlayerByName(playerName, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion


module.exports = router;