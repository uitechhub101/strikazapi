const INCLUDES = "include"
const SEASONS = "seasons"
const TEAMS = "teams"
const TEAM_ID = "TEAM_ID"
const DATE = "DATE"
const TEAM_NAME = "TEAM_NAME"
const SEASON_ID = "SEASON_ID"
const LEAGUE_NAME = "LEAGUE_NAME"
const LEAGUE_ID = "LEAGUE_ID"
const API_TOKEN = "api_token"
const BOOK_MAKERS = "bookmakers"
const LEAGUES = "leagues"
const FIXTURES = "fixtures"
const STATUS = "status"
const MARKETS = "markets"
const COUNTRY_ID = "COUNTRY_ID"
const PLAYER_ID = "PLAYER_ID"
const PLAYER_NAME = "PLAYER_NAME"
const START_DATE = "START_DATE"
const END_DATE = "END_DATE"
const FIXTURE_ID = "FIXTURE_ID"
const MARKET_ID = "MARKET_ID"
const GROUP_IDS = "GROUP_IDS"
const STAGE_IDS = "STAGE_IDS"
const DATE_TIME = "DATE_TIME"


module.exports = {
    INCLUDES, SEASONS, TEAMS, TEAM_ID, TEAM_NAME,
    DATE, SEASON_ID, LEAGUES, LEAGUE_NAME, LEAGUE_ID, 
    API_TOKEN, BOOK_MAKERS, FIXTURES, FIXTURE_ID,
    STATUS, MARKETS, COUNTRY_ID, PLAYER_ID, PLAYER_NAME,
    START_DATE, END_DATE, MARKET_ID, GROUP_IDS, STAGE_IDS, DATE_TIME
}