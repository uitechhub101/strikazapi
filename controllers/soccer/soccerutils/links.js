const pathParams = require('./path.parameters');

// todo - add other links here
const SoccerLinks = {
    allLeagues: String = `v2.0/leagues`,
    allSeasons: String = `v2.0/seasons`,
    leagueById: String = `v2.0/leagues/{${pathParams.LEAGUE_ID}}`,
    leaguesByCountryId: String = `v2.0/countries/{${pathParams.COUNTRY_ID}}/leagues`,
    leagueByName: String = `v2.0/leagues/search/{${pathParams.LEAGUE_NAME}}`,
    seasonById: String = `v2.0/seasons/{${pathParams.SEASON_ID}}`,
    teamById: String = `v2.0/teams/{${pathParams.TEAM_ID}}`,
    teamsBySeasonId: String = `v2.0/teams/season/{${pathParams.SEASON_ID}}`,
    teamsByCountryId: String = `v2.0/teams/season/{${pathParams.COUNTRY_ID}}`,
    teamCurrentLeaguesById: String = `v2.0/teams/{${pathParams.TEAM_ID}}/current`,
    teamHistoricalLeaguesById: String = `v2.0/teams/{${pathParams.TEAM_ID}}/history`,
    teamByName: String = `v2.0/teams/search/{${pathParams.TEAM_NAME}}`,
    allLiveScores: String = `v2.0/livescores`,
    liveScoresNow: String = `v2.0/livescores/now`,
    allMarkets: String = `v2.0/markets`,
    marketsById: String = `v2.0/markets/{${pathParams.MARKET_ID}}`,
    allFixturesByMarketsId: String = `v2.0/markets/{${pathParams.MARKET_ID}}/fixtures`,
    standingsBySeasonId: String = `v2.0/standings/season/{${pathParams.SEASON_ID}}`,
    standingsBySeasonIdAndDate: String = `v2.0/standings/season/{${pathParams.SEASON_ID}}/date/{${pathParams.DATE_TIME}}`,
    standingsCorrectionsBySeasonId: String = `v2.0/corrections/season/{${pathParams.SEASON_ID}}`,
    playerById: String = `v2.0/players/{${pathParams.PLAYER_ID}}`,
    playerByName: String = `v2.0/players/search/{${pathParams.PLAYER_NAME}}`,
    playersForCountryId: String = `v2.0/countries/{${pathParams.COUNTRY_ID}}/players`,
    fixturesByDate: String = `v2.0/fixtures/date/{${pathParams.DATE}}`,
    fixturesByDateRange: String = `v2.0/fixtures/between/{${pathParams.START_DATE}}/{${pathParams.END_DATE}}`,
    fixturesByDateRangeForTeam: String = `v2.0/fixtures/between/{${pathParams.START_DATE}}/{${pathParams.END_DATE}}/{${pathParams.TEAM_ID}}`,
    fixturesById: String = `v2.0/fixtures/{${pathParams.FIXTURE_ID}}`,
    lastUpdatedFixtures: String = `v2.0/fixtures/updates`,
};

const ConfigLinks = {
    appConfig: String = `v1.0/appConfig`
};

const replacePathParams = (urlString, param, value) => {
    return urlString.replace(param, value);
};

/**
 * 
 * Function that creates link query params from supplied param.
 * @param {Map<String, any} params representing query params key and value.
 */
const appendQueryParams = (params) => {
    var result = "";
    if (params) {
        var appendString = "?";
        for (var [key, value] of params.entries()) {
            appendString += `{${key}={${value}&`  
        }
        result += appendString.substring(0, appendString.length - 1)
    }
    return result;
}

module.exports = {
    appendQueryParams,
    SoccerLinks,
    ConfigLinks,
    replacePathParams
};