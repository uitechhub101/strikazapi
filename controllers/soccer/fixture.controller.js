const { SoccerLinks } = require('./soccerutils/links');
const pathParams = require('../soccer/soccerutils/path.parameters');
const router = require('express').Router();
const SportmonksApi = require('./soccerutils/sports.monks').SportmonksApi

router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
});

const token = process.env.SOCCER_API_TOKEN
//region getFixturesById
const getFixturesById = (fixtureId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        localTeam: true,
        visitorTeam: true,
        lineup: true,
        localCoach: true,
        visitorCoach: true,
        referee: true,
        venue: true
    };
    queryParams[pathParams.FIXTURE_ID] = fixtureId;
    api.get(SoccerLinks.fixturesById, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
}

router.route('/id/:fixtureId').get((req, res) => {
    const { fixtureId } = req.params;
    getFixturesById(fixtureId, (response, error) => {
        if (response) {
            // console.log(`response for getFixturesById gotten ${response}`);
            res.header["Content-Type"] = "application/json"
            res.end(response);
        } else if (error) {
            res.end('Error ',error);
        }
    });

});
//endregion

//region getLastUpdatedFixtures
const getLastUpdatedFixtures = (apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        localTeam: true,
        visitorTeam: true,
        lineup: true,
        localCoach: true,
        visitorCoach: true,
        referee: true,
        venue: true
    };
    api.get(SoccerLinks.lastUpdatedFixtures, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/lastUpdated').get((req, res) => {
    getLastUpdatedFixtures((response, error) => {
        if (response) {
            // console.log(`response for getLastUpdatedFixtures gotten ${response}`);
            res.end(response);
        } else if (error) {
            res.end("Error ", error);
        }
    })
});
//endregion

//region getFixturesByDate
const getFixturesByDate = (date, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        localTeam: true,
        visitorTeam: true,
        lineup: true,
        localCoach: true,
        visitorCoach: true,
        referee: true,
        venue: true
    };
    queryParams[pathParams.DATE] = date;
    api.get(SoccerLinks.fixturesByDate, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/date/:date').get((req, res) => {
    const { date } = req.params;
    getFixturesByDate(date, (response, error) => {
        if (response) {
            // console.log(`response for getFixturesByDate gotten ${response}`);
            res.end(response);
        } else if (error) {
            // console.log(`error for getFixturesByDate gotten ${response}`);
            res.end("Error ", error);
        }
    })
});
//endregion

//region getFixturesByDateRange
const getFixturesByDateRange = ( startDate, endDate, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        localTeam: true,
        visitorTeam: true,
        lineup: true,
        localCoach: true,
        visitorCoach: true,
        referee: true,
        venue: true
    };
    queryParams[pathParams.START_DATE] = startDate;
    queryParams[pathParams.END_DATE] = endDate;
    api.get(SoccerLinks.fixturesByDateRange, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/startDate/:start/endDate/:end').get((req, res) => {
    const { start, end } = req.params;
    getFixturesByDateRange(start, end, (response, error) => {
        if (response) {
            // console.log(`response for getFixturesByDateRange gotten ${response}`);
            res.end(response);
        } else if (error) {
            // console.log(`error for getFixturesByDateRange gotten ${response}`);
            res.end("Error ", error);
        }
    })
});
//endregion

//region getFixturesByDateRangeForTeam
const getFixturesByDateRangeForTeam = ( endDate, startDate, teamId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        localTeam: true,
        visitorTeam: true,
        lineup: true,
        localCoach: true,
        visitorCoach: true,
        referee: true,
        venue: true
    };
    queryParams[pathParams.TEAM_ID] = teamId;
    queryParams[pathParams.END_DATE] = endDate;
    queryParams[pathParams.START_DATE] = startDate;
    api.get(SoccerLinks.fixturesByDateRangeForTeam, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/startDate/:start/endDate/:end/teamId/:teamId').get((req, res) => {
    const { end, start, teamId } = req.params;
    getFixturesByDateRangeForTeam(end, start, teamId, (response, error) => {
        if (response) {
            // console.log(`response for getFixturesByDateRangeForTeam gotten ${response}`);
            res.end(response);
        } else if (error) {
            // console.log(`error for getFixturesByDateRangeForTeam gotten ${response}`);
            res.end("Error ", error);
        }
    })
});
//endregion

module.exports = router;