const { SoccerLinks } = require('./soccerutils/links');
const pathParams = require('../soccer/soccerutils/path.parameters');
const router = require('express').Router();
const SportmonksApi = require('./soccerutils/sports.monks').SportmonksApi

router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
});

const token = process.env.SOCCER_API_TOKEN

//region getAllSeasonsForPlan
const getAllSeasonsForPlan = (apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        // fixtures: true, // adding this include triples response time(+-568ms to 3.52secs) and payload size (24.14kb to 8.83 mb).
        league: true
    };
    api.get(SoccerLinks.allSeasons, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
};

router.route('/all').get((req, res) => {
    getAllSeasonsForPlan((response, error) => {
        if (response) {
            // console.log(`response for getSeasonById gotten ${response}`);
            res.header["Content-Type"] = "application/json"
            res.end(response);
        } else if (error) {
            console.log('error ', error);
            res.end('Error ',error);
        }
    });

});
//endregion

//region getSeasonById
const getSeasonById = (seasonId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        results: true,
        fixtures: true
    };
    queryParams[pathParams.SEASON_ID] = seasonId;
    api.get(SoccerLinks.seasonById, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch(error => {
            apiResponseCallback(null, error);
        });
}

router.route('/id/:seasonId').get((req, res) => {
    const { seasonId } = req.params;
    getSeasonById(seasonId, (response, error) => {
        if (response) {
            // console.log(`response for getSeasonById gotten ${response}`);
            res.header["Content-Type"] = "application/json"
            res.end(response);
        } else if (error) {
            res.end('Error ',error);
        }
    });

});
//endregion

module.exports = router;