const { SoccerLinks } = require('./soccerutils/links');
const pathParams = require('../soccer/soccerutils/path.parameters');
const router = require('express').Router();
const SportmonksApi = require('./soccerutils/sports.monks').SportmonksApi

router.use((req, res, next) => {
    console.log('Time: ', Date.now())
    next()
});

const token = process.env.SOCCER_API_TOKEN
// region getTeamById
const getTeamById = (teamId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        coach: true,
        league: true,
        venue: true
    };
    queryParams["squad.player"] = true;
    queryParams[pathParams.TEAM_ID] = teamId;
    api.get(SoccerLinks.teamById, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch((error) => {
            apiResponseCallback(null, error);
        });
};

router.route('/:teamId').get((req, res) => {
    const { teamId } = req.params;
    getTeamById(teamId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

// region getTeamsByCountryId
const getTeamsByCountryId = (countryId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        league: true,
        latest: true,
        activeSeason: true
    };
    queryParams[pathParams.COUNTRY_ID] = countryId;
    api.get(SoccerLinks.teamsByCountryId, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch((error) => {
            apiResponseCallback(null, error);
        });
};

router.route('/countryId/:countryId').get((req,res) => {
    const { countryId } = req.params;
    getTeamsByCountryId(countryId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});

//endregion

// region getTeamBySeasonId
const getTeamBySeasonId = (seasonId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        coach: true,
        league: true,
        venue: true
    };
    queryParams[pathParams.SEASON_ID] = seasonId;
    api.get(SoccerLinks.teamsBySeasonId, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch((error) => {
            apiResponseCallback(null, error);
        });
};

router.route('/seasonId/:seasonId').get((req, res) => {
    const { seasonId } = req.params;
    getTeamBySeasonId(seasonId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

// region getTeamByName
const getTeamByName = (name, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        coach: true,
        league: true
    };
    queryParams['latest:limit(5|1)'] = true;
    queryParams['squad.player'] = true;
    queryParams[pathParams.TEAM_NAME] = name;
    api.get(SoccerLinks.teamByName, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch((error) => {
            apiResponseCallback(null, error);
        });
};

router.route('/name/:name').get((req, res) => {
    const { name } = req.params;
    getTeamByName(name, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

//region getTeamDetails
const getTeamDetails = (teamId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {
        rivals: true
    }
    queryParams['latest:limit(5|1)'] = true;
    queryParams['upcoming:limit(5|1)'] = true;
    queryParams[pathParams.TEAM_ID] = teamId;
    api.get(SoccerLinks.teamById, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch((error) => {
            apiResponseCallback(null, error)
        });
};

router.route('/:teamId/details').get((req, res) => {
    const { teamId } = req.params;
    getTeamDetails(teamId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

// region getCurrentLeaguesByTeamId
const getTeamLeaguesById = (teamId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {};
    queryParams[pathParams.TEAM_ID] = teamId;
    api.get(SoccerLinks.teamCurrentLeaguesById, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch((error) => {
            apiResponseCallback(null, error);
        });
};

router.route('/currentLeagues/:teamId').get((req, res) => {
    const { teamId } = req.params;
    getTeamLeaguesById(teamId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

// region getAllLeaguesByTeamId
const getTeamHistoricalLeaguesById = (teamId, apiResponseCallback) => {
    const api = new SportmonksApi(token);
    const queryParams = {};
    queryParams[pathParams.TEAM_ID] = teamId;
    api.get(SoccerLinks.teamHistoricalLeaguesById, queryParams)
        .then((response) => {
            apiResponseCallback(JSON.stringify(response), null);
        })
        .catch((error) => {
            apiResponseCallback(null, error);
        });
};

router.route('/historicalLeagues/:teamId').get((req, res) => {
    const { teamId } = req.params;
    getTeamHistoricalLeaguesById(teamId, (response, error) => {
        if (response) {
            res.end(response);
        } else if (error) {
            res.end('Error ', error);
        }
    });
});
//endregion

module.exports = router;