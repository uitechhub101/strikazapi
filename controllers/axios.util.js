const axios = require('axios').default;
const pathParams = require('./soccer/soccerutils/path.parameters');
const baseV2Url = require('./soccer/soccerutils/links');

const soccerAPIHeaders = new Map();
soccerAPIHeaders.set(pathParams.API_TOKEN, process.env.SOCCER_API_TOKEN);
const soccerAPIAxios = axios.create({
    baseURL: baseV2Url,
    timeout: 2500,
    responseType: 'json',
    responseEncoding: 'utf8',
    headers: soccerAPIHeaders
});

const soccerResponseInterceptor = soccerAPIAxios.interceptors.response.use(function (response) {
    // todo - handle caching response here.
    // for now, just return as normal.
    return response;
}, function (error) {
    return Promise.reject(error);
});

const deleteCall = (url, body, callback) => {
    soccerAPIAxios.delete(url, { data: body })
        .then(response => callback(response, null))
        .catch(error => callback(null, error));
};

const getCall = (url, callback) => {
    soccerAPIAxios.get(url)
        .then(response => callback(response, null))
        .catch(error => callback(null, error));
};

const postCall = (url, body, callback) => {
    soccerAPIAxios.post(url, { data: body })
        .then(response => callback(response, null))
        .catch(error => callback(null, error));
};

const putCall = (url, body, callback) => {
    soccerAPIAxios.put(url, { data: body })
        .then(response => callback(response, null))
        .catch(error => callback(null, error));
}


module.exports = {
    getCall,
    putCall,
    postCall,
    deleteCall
};