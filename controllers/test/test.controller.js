const Test = require('../schema/test.model');
const router = require('express').Router();
const fs = require('fs');
const html = fs.readFileSync('./html/index.html');
var log = function(entry) {
    fs.appendFileSync('/tmp/strikazapi-app.log', new Date().toISOString() + ' - ' + entry + '\n');
    console.log(entry);
};

router.route('/all').get((req, res) => {
    log(`Received message: ${req.body}`)
    Test.find()
        .then(results => res.status(200).json(results))
        .catch(err => res.status(400).json("Error => "+ err));
});

router.route('/').get((req, res) => {
    console.log("Hit TestControllers get call");
    log(`Received message: ${req.body}`)
    res.writeHead(200);
    res.write(html);
    res.end();
});

router.route('/new').post((req, res) => {
    log(`Received message: ${req.body}`)
    const newTest = new Test(req.body);
    newTest.time = Date();
    newTest.markModified("time");
    newTest.save()
        .then(test => res.status(201).json(test))
        .catch(err => res.status(400).json("Error! " + err));
});

module.exports = router