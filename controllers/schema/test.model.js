const mongoose = require('mongoose')
const Schema = mongoose.Schema;
const testSchema = new Schema({
    testName: String,
    email: String,
    age: Number,
    time: Date
});
const Test = mongoose.model('Test', testSchema);
module.exports = Test