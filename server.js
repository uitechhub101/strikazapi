const express = require('express');
const morgan = require('morgan');
const helmet = require('helmet');
const mongoose = require('mongoose');
const rateLimit = require('express-rate-limit');
const testRoutes = require('./controllers/test/test.controller');
const fixtureRoutes = require('./controllers/soccer/fixture.controller');
const seasonRoutes = require('./controllers/soccer/season.controler');
const standingRoutes = require('./controllers/soccer/standings.controller');
const teamRoutes = require('./controllers/soccer/team.controller');
const playerRoutes = require('./controllers/soccer/player.controller');
const leagueRoutes = require('./controllers/soccer/league.controller');

const databaseUri = process.env.DATABASE_URI;
if (!databaseUri) {
    console.log('DATABASE_URI not specified, falling back to localhost.');
} else {
    console.log(`DATABASE_URI: ==>  ${databaseUri}`);
}
// start db connection first.
// mongoose.connect(databaseUri, {
//     useNewUrlParser: true,
//     useUnifiedTopology: true
// }, err => {
//     if (err) {
//         console.log('Erred but still Connected to MongoDB!!!', err);
//     }
// });
// const connection = mongoose.connection;
// connection.once('open', () => {
//     console.log('Mongoose connected to DB! 🚀');
// });

const limiter = rateLimit({
    windowMs: 15 * 60 * 1000, // 15minutes
    max: 100,
    message: "Too many requests to the same endpoint, please wait 2 minutes and try again."
});

const app = express();
app.use(express.json());
// app.use(morgan("dev"));
app.use(helmet());
// app.use(limiter); // temporarily turn off limiter.
app.use('/test', testRoutes);
app.use('/fixtures', fixtureRoutes);
app.use('/season', seasonRoutes);
app.use('/standing', standingRoutes);
app.use('/team', teamRoutes);
app.use('/players', playerRoutes);
app.use('/league', leagueRoutes);
// For invalid routes
app.get('*', (req, res) => {
    const msg = "Invalid route ->" + req.path;
    console.log(msg);
    res.status(400);
    res.end('404! '+msg);
});

const PORT = process.env.PORT || 1337;
app.listen(PORT, () => {
    console.log('Server started!! 🚀 🚀 🚀')
});

module.exports = {
    app
};